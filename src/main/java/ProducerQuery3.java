import Utils.FileUtils;
import Utils.StringUtils;
import kafka.ProducerTemporizzato;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class ProducerQuery3 {


    public static String BOOTSTRAP_SERVERS =
            "35.225.218.158:9092";


    public static void main(String[] args) throws IOException {
        String pathFolder="/Users/Valerio/Downloads/data/";
        ArrayList<String> friends= FileUtils.readFileBat(pathFolder+"friendships.dat");
        ArrayList<String> posts=FileUtils.readFileBat(pathFolder+"posts.dat");
        ArrayList<String> comments=FileUtils.readFileBat(pathFolder+"comments.dat");

        run(friends,posts,comments);

    }


    public static  void run(ArrayList<String> friends,ArrayList<String> posts,ArrayList<String> comments) {


        final org.apache.kafka.clients.producer.Producer<Long, String> producer = createProducer();
        try {
            int indexFriends=0,indexPosts=0,indexComments=0;
            while (true) {
                if (indexFriends>=friends.size() && indexPosts>=posts.size() && indexComments>=comments.size())
                    break;

                String s1=new String();
                String s2=new String();
                String s3=new String();

                if (indexFriends<friends.size()) {
                    s1 = friends.get(indexFriends);

                }
                if (indexPosts< posts.size()) {
                    s2 = posts.get(indexPosts);
                }
                if (indexComments<comments.size()) {
                    s3 = comments.get(indexComments);
                }


                ArrayList<String> arrayList=StringUtils.getMinDate(s1,s2,s3);
                String TOPIC=new String();

                if (arrayList.get(0).equals(s1)){
                    TOPIC="Friends";
                    indexFriends++;
                }
                else if (arrayList.get(0).equals(s2)){
                    TOPIC="Posts";
                    indexPosts++;
                }
                else{
                    TOPIC="Comments";
                    indexComments++;

                }

                    final ProducerRecord<Long, String> record = new ProducerRecord<>(TOPIC, arrayList.get(0));
                    try {


                        RecordMetadata metadata = producer.send(record).get();

                        System.out.printf("sent record(topic=%s key=%s value=%s) " + "meta(partition=%d, offset=%d)\n", TOPIC, record.key(), record.value(), metadata.partition(), metadata.offset());

                    }catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }



            }

        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            producer.flush();
            producer.close();
        }
    }

    private static org.apache.kafka.clients.producer.Producer<Long, String> createProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                BOOTSTRAP_SERVERS);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class.getName());
        return new KafkaProducer<>(props);
    }
}
