import Utils.FileUtils;
import Utils.StringUtils;
import kafka.ProducerTemporizzato;
import neo4j.HelloWorldExample;
import redis.clients.jedis.Jedis;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class MainProducer {

    public static String TOPIC = "test";
    public static String BOOTSTRAP_SERVERS =
                   "35.225.218.158:9092";

    public static void main(String[] args) throws InterruptedException, IOException {
        try {

            //org.apache.log4j.BasicConfigurator.configure();
            String string="2010-02-10T11:20:20.785+0000|529590|2886|LOL|Baoping Wu||529361";
            System.out.println(string.split("\\|").length);

            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
            Date date;
            date = inputFormat.parse("2010-02-10T04:05:20.779+0000");
            System.out.println(date);
            System.out.println(date.getTime());
            Calendar calendar=GregorianCalendar.getInstance();
            calendar.setTime(date);
            System.out.println("giorno "+calendar.get(Calendar.DAY_OF_MONTH)+" mese "+calendar.get(Calendar.MONTH)+1+" anno "+calendar.get(Calendar.YEAR));
            SimpleDateFormat outputFormat = new SimpleDateFormat("MMM_dd_yyyy");
            System.out.println(outputFormat.format(date));
        }catch(Exception e){
            //cannot happen in this example
        }


        //String pathFolder="/usr/src/myapp/";
        String pathFolder="/Users/Valerio/Downloads/data/";




        try {
            ArrayList<String> friends= FileUtils.readFileBat(pathFolder+"friendships.dat");
            Thread thread=new Thread(new ProducerTemporizzato("Friends",BOOTSTRAP_SERVERS,friends,GLOBAL.scala));
            thread.start();
            ArrayList<String> posts=FileUtils.readFileBat(pathFolder+"posts.dat");
            Thread thread1=new Thread(new ProducerTemporizzato("Posts",BOOTSTRAP_SERVERS,posts,GLOBAL.scala));
            thread1.start();

            ArrayList<String> comments=FileUtils.readFileBat(pathFolder+"comments.dat");
            Thread thread2=new Thread(new ProducerTemporizzato("Comments",BOOTSTRAP_SERVERS,comments,GLOBAL.scala));
            thread2.start();

            /*Thread thread3=new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("Scrivo su kafka i tre dataset");
                    while(true){
                        System.out.print(".");
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            thread3.start();*/

            /*thread.join();
            thread1.join();
            thread2.join();*/
            //thread3.interrupt();
            //System.out.println("finito!");

        } catch (Exception e) {
            e.printStackTrace();
        }

       /* new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    kafka.Consumer.runConsumer(BOOTSTRAP_SERVERS,"Friends");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();*/

        /*new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    kafka.Consumer.runConsumer(BOOTSTRAP_SERVERS,TOPIC2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();*/


        /*//Connecting to Redis server on localhost
        Jedis jedis = new Jedis("localhost");
        System.out.println("Connection to server sucessfully");
        System.out.println();
        //store data in redis list
        jedis.lpush("prova2","prova3");
        jedis.lpush("tutorial-list", "Redis");
        jedis.lpush("tutorial-list", "Mongodb");
        jedis.lpush("tutorial-list", "Mysql");
        // Get the stored data and print it
        List<String> list = jedis.lrange("tutorial-list", 0 ,5);
        jedis.lrange("prova2", 0 ,5).forEach(s -> System.out.println("Stored string in redis "+s));


        list.forEach(s -> System.out.println("Stored string in redis: "+s));


        //Connecting to Redis server on localhost
        //store data in redis list
        // Get the stored data and print it
        Set<String> list2 = jedis.keys("*");

        list2.forEach(s -> {
                System.out.println(s);
                jedis.lrange(s, 0, 5).forEach(s1 -> System.out.println(s1));
        });*/



    }






}
