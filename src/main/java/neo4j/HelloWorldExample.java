package neo4j;

import org.neo4j.driver.v1.*;

import static org.neo4j.driver.v1.Values.parameters;

public class HelloWorldExample implements AutoCloseable{
    private final Driver driver;

    public HelloWorldExample( String uri, String user, String password )
    {
        driver = GraphDatabase.driver( uri, AuthTokens.basic( user, password ) );
    }

    @Override
    public void close() throws Exception {
        driver.close();
    }

    public void printGreeting( final String persona1,final String persona2 )
    {
        try ( Session session = driver.session() )
        {
            String greeting = session.writeTransaction( new TransactionWork<String>()
            {
                @Override
                public String execute( Transaction tx )
                {
                    StatementResult result=tx.run("MATCH (a:Persona {nome: $persona}) RETURN a",parameters("persona",persona1));
                    if (result.hasNext()){
                        System.out.println("nodo già esistente");
                        //System.out.println(result.list().get(0));
                    }
                    else {
                        tx.run("CREATE (a:Persona {nome: $persona})",parameters("persona",persona1));
                    }

                    result=tx.run("MATCH (a:Persona {nome: $persona}) RETURN a",parameters("persona",persona2));
                    if (result.hasNext()){
                        System.out.println("nodo già esistente");
                        //System.out.println(result.list().get(0));
                    }
                    else {
                        tx.run("CREATE (a:Persona {nome: $persona})",parameters("persona",persona2));
                    }

                    tx.run("MATCH (a:Persona {nome: $persona1}),(b:Persona {nome: $persona2})" +
                            " CREATE (a)-[:CONOSCE]->(b),(b)-[:CONOSCE]->(a)",parameters("persona1",persona1,"persona2",persona2));

                    return null;
                }
            } );
            //System.out.println( greeting );
        }
    }

    public static void main( String... args ) throws Exception
    {
        try ( HelloWorldExample greeter = new HelloWorldExample( "bolt://localhost:7687", "neo4j", "admin" ) )
        {
            greeter.printGreeting( "gianni","pinotto" );
        }
    }
}
