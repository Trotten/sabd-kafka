package Utils;

import javax.xml.crypto.Data;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StringUtils {

    public static Date getDateFromString(String string) throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        Date date;
        return inputFormat.parse(string);
    }

    public static String GetSubStringPipe(String string,int start){
        int end=string.indexOf("|",start);
        return string.substring(start,end);
    }

    public static ArrayList<String> getMinDate(String s1, String s2, String s3) throws ParseException {

        ArrayList<String> arrayList=new ArrayList<>();
        Long date1= getDateFromString(s1.split("\\|")[0]).getTime();
        Long min=date1;
        Long date2= getDateFromString(s2.split("\\|")[0]).getTime();
        if (date2<min)
            min=date2;
        Long date3= getDateFromString(s3.split("\\|")[0]).getTime();
        if (date3<min)
            min=date3;
        if (date1==min){
            arrayList.add(s1);
            if (date2<date3) {
                arrayList.add(s2);
                arrayList.add(s3);
            }
            else {
                arrayList.add(s3);
                arrayList.add(s2);
            }
        }
        else if (date2==min)
        {
            arrayList.add(s2);
            if (date1<date3){
                arrayList.add(s1);
                arrayList.add(s3);
            }
            else{
                arrayList.add(s3);
                arrayList.add(s1);
            }
        }
        else if (date3==min){
            arrayList.add(s3);
            if (date1<date2){
                arrayList.add(s1);
                arrayList.add(s2);
            }
            else{
                arrayList.add(s2);
                arrayList.add(s1);
            }
        }

    return arrayList;
    }
}
