package Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FileUtils {

    public static ArrayList<String> readFileBat(String path) throws IOException {
        ArrayList<String> arrayList=new ArrayList<>();
        FileReader file = new FileReader(new File(path));
        BufferedReader br = new BufferedReader(file);
        String temp = br.readLine();
        while (temp != null) {
            arrayList.add(temp);
            temp = br.readLine();
        }
        return arrayList;
    }


}
