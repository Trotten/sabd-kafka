package kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;

public class Producer {

    private static org.apache.kafka.clients.producer.Producer<Long, String> createProducer(String BOOTSTRAP_SERVERS) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                BOOTSTRAP_SERVERS);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class.getName());
        return new KafkaProducer<>(props);
    }

    public static void runProducer(String TOPIC, String BOOTSTRAP_SERVERS, ArrayList<String> arrayList) throws Exception {
        final org.apache.kafka.clients.producer.Producer<Long, String> producer = createProducer(BOOTSTRAP_SERVERS);
        try {
            arrayList.forEach(s -> {
                final ProducerRecord<Long, String> record = new ProducerRecord<>(TOPIC,s);
                try {
                    RecordMetadata metadata = producer.send(record).get();
                    System.out.printf("sent record(key=%s value=%s) " + "meta(partition=%d, offset=%d)\n",
                            record.key(), record.value(), metadata.partition(), metadata.offset());

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            });
        } finally {
            producer.flush();
            producer.close();
        }
    }




    /*static void runProducer(String TOPIC,String BOOTSTRAP_SERVERS,final int sendMessageCount) throws Exception {
        final org.apache.kafka.clients.producer.Producer<Long, String> producer = createProducer(BOOTSTRAP_SERVERS);
        long time = System.currentTimeMillis();
        try {
            for (long index = time; index < time + sendMessageCount; index++) {
                final ProducerRecord<Long, String> record =
                        new ProducerRecord<>(TOPIC, index,"Hello Mom " + index);
                RecordMetadata metadata = producer.send(record).get();
                long elapsedTime = System.currentTimeMillis() - time;
                System.out.printf("sent record(key=%s value=%s) " +
                                "meta(partition=%d, offset=%d) time=%d\n",
                        record.key(), record.value(), metadata.partition(),
                        metadata.offset(), elapsedTime);
            }
        } finally {
            producer.flush();
            producer.close();
        }
    }*/
}
