package kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Properties;

public class Consumer {

    private static org.apache.kafka.clients.consumer.Consumer<Long, String> createConsumer(String BOOTSTRAP_SERVERS,String TOPIC) {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG,
                "KafkaExampleConsumer");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                LongDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class.getName());
        // Create the consumer using props.
        final org.apache.kafka.clients.consumer.Consumer<Long, String> consumer =
                new KafkaConsumer<>(props);
        // Subscribe to the topic.
        consumer.subscribe(Collections.singletonList(TOPIC));
        return consumer;
    }

    public static void runConsumer(String BOOTSTRAP_SERVERS,String TOPIC) throws InterruptedException {
        final org.apache.kafka.clients.consumer.Consumer<Long, String> consumer = createConsumer(BOOTSTRAP_SERVERS, TOPIC);
        final int giveUp = 100;   int noRecordsCount = 0;
        while (true) {
                ConsumerRecords<Long, String> consumerRecords=
                        consumer.poll(1000);
           /* if (consumerRecords.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }*/
           if (consumerRecords.isEmpty()){
               //System.out.println("vuoto");
               /*consumer.seekToEnd(new Collection<TopicPartition>() {
                   @Override
                   public int size() {
                       return 0;
                   }

                   @Override
                   public boolean isEmpty() {
                       return false;
                   }

                   @Override
                   public boolean contains(Object o) {
                       return false;
                   }

                   @Override
                   public Iterator<TopicPartition> iterator() {
                       return null;
                   }

                   @Override
                   public Object[] toArray() {
                       return new Object[0];
                   }

                   @Override
                   public <T> T[] toArray(T[] a) {
                       return null;
                   }

                   @Override
                   public boolean add(TopicPartition topicPartition) {
                       return false;
                   }

                   @Override
                   public boolean remove(Object o) {
                       return false;
                   }

                   @Override
                   public boolean containsAll(Collection<?> c) {
                       return false;
                   }

                   @Override
                   public boolean addAll(Collection<? extends TopicPartition> c) {
                       return false;
                   }

                   @Override
                   public boolean removeAll(Collection<?> c) {
                       return false;
                   }

                   @Override
                   public boolean retainAll(Collection<?> c) {
                       return false;
                   }

                   @Override
                   public void clear() {

                   }
               });
               */
           }
           else {
               consumerRecords.forEach(record -> {
                   System.out.printf("TOPIC: %s Consumer Record:(%d, %s, %d, %d)\n",
                           TOPIC,
                           record.key(), record.value(),
                           record.partition(), record.offset());
               });
               consumer.commitAsync();
           }

        }
        //consumer.close();
        //System.out.println("DONE");
    }

}
