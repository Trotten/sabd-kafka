package kafka;

import Utils.FileUtils;
import Utils.StringUtils;
import neo4j.HelloWorldExample;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class ProducerTemporizzato implements Runnable {


    private  String TOPIC;
    private static String BOOTSTRAP_SERVERS;
    private ArrayList<String> arrayList;
    private long scala;

    public ProducerTemporizzato(String TOPIC, String BOOTSTRAP_SERVERS, ArrayList<String> arrayList,long scala) {
        this.TOPIC = TOPIC;
        this.BOOTSTRAP_SERVERS = BOOTSTRAP_SERVERS;
        this.arrayList = arrayList;
        //this.thr.start();
        this.scala=scala;
    }

    public String getTOPIC() {
        return TOPIC;
    }

    public void setTOPIC(String TOPIC) {
        this.TOPIC = TOPIC;
    }

    public String getBOOTSTRAP_SERVERS() {
        return BOOTSTRAP_SERVERS;
    }

    public void setBOOTSTRAP_SERVERS(String BOOTSTRAP_SERVERS) {
        this.BOOTSTRAP_SERVERS = BOOTSTRAP_SERVERS;
    }

    public ArrayList<String> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<String> arrayList) {
        this.arrayList = arrayList;
    }
    @Override
    public void run() {

       /* //neo4j
        HelloWorldExample helloWorldExample=null;
        if (TOPIC.equals("Friends")){
            helloWorldExample=new HelloWorldExample("bolt://localhost:7687","neo4j","admin");
        }
        //neo4j*/

        final org.apache.kafka.clients.producer.Producer<Long, String> producer = createProducer();
        try {
            final Date[] lastDate = {null};

            /*//neo4j
            HelloWorldExample finalHelloWorldExample = helloWorldExample;
            //neo4j
*/

            arrayList.forEach(s -> {
                final ProducerRecord<Long, String> record = new ProducerRecord<>(TOPIC,s);
                try {
                    //temporizzazione
                    Date newDate= StringUtils.getDateFromString(StringUtils.GetSubStringPipe(s,0));

                   /* //neo4j
                    if (TOPIC.equals("Friends")){
                        String[] strings=s.split("\\|");
                        finalHelloWorldExample.printGreeting(strings[1],strings[2]);
                    }

                    //fine neo4j*/

                    //temporizzazione
                    /*if (lastDate[0] ==null) {

                        lastDate[0] =new Date(newDate.getTime());
                    }
                    else {
                        long timeToSleep=(Math.abs(newDate.getTime()-lastDate[0].getTime()))/scala;
                        //System.out.println(timeToSleep);
                        Thread.sleep(timeToSleep);
                    }*/

                    //fine temporizzazione

                    RecordMetadata metadata = producer.send(record).get();

                    System.out.printf("sent record(topic=%s key=%s value=%s) " + "meta(partition=%d, offset=%d)\n", TOPIC,record.key(), record.value(), metadata.partition(), metadata.offset());

                    lastDate[0]=new Date(newDate.getTime());

                }catch (ParseException e) {
                    e.printStackTrace();
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            });

        } finally {
            producer.flush();
            producer.close();
        }
    }

    private static org.apache.kafka.clients.producer.Producer<Long, String> createProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                BOOTSTRAP_SERVERS);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class.getName());
        return new KafkaProducer<>(props);
    }



}
