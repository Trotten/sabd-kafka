package kafkaStream;

import Utils.StringUtils;
import org.apache.kafka.common.protocol.types.Field;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import reactor.util.function.Tuple2;


import java.text.ParseException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Query1 {


    //KafkaStreamProperties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,"35.225.218.158:9092");
      //  KafkaStreamProperties.put(StreamsConfig.ZOOKEEPER_CONNECT_CONFIG,"35.193.244.179:2181");
    private static Properties createStreamProperties(){
        final Properties props = new Properties();

        // Give the Streams application a unique name.
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "wordcount-lambda-example");
        props.put(StreamsConfig.CLIENT_ID_CONFIG, "wordcount-lambda-example-client");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "35.225.218.158:9092");

        // Specify default (de)serializers for record keys and for record values.
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,
                Serdes.Long().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,
                Serdes.String().getClass().getName());

        // Records should be flushed every 10 seconds.
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 10 * 1000);
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);

        return props;
    }

    public static void main(String[] args) {


        final Properties props = createStreamProperties();
        final StreamsBuilder builder = new StreamsBuilder();


        // Construct a `KStream` from the input topic "streams-plaintext-input",
        final KStream<Long, String> textLines =
                builder.stream("Friends");


        textLines
                .map(new KeyValueMapper<Long, String, KeyValue<Long, String>>() {
                    @Override
                    public KeyValue<Long, String> apply(Long aLong, String s) {
                        Long timestamp=null;
                        try {
                            timestamp=StringUtils.getDateFromString(s.split("\\|")[0]).getTime();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Calendar calendar= GregorianCalendar.getInstance();
                        calendar.setTime(new Date(timestamp));
                        return new KeyValue<Long, String>(Long.valueOf(calendar.get(Calendar.HOUR_OF_DAY)),"1");
                    }
                })
                .groupByKey()
                //.windowedBy(SessionWindows.with(TimeUnit.SECONDS.toSeconds(1)))
                .count()
                .toStream()
                .mapValues(aLong -> String.valueOf(aLong))
                .to(Serdes.Long(),Serdes.String(),"Query1");


        /*KTable<String, String> output=textLines
                .mapValues(s -> {
                    try {
                        return StringUtils.getDateFromString(s.toString().split("\\|")[0]).getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .selectKey((s, aLong) -> aLong.toString())
                .mapValues(aLong -> 1L)
                .groupByKey()
                .reduce((aLong, v1) -> aLong+v1)
                .mapValues((s, aLong) -> {
                    try {
                        return "timestamp: " + s + " valore " + aLong;
                    }
                catch (Exception e){
                    //e.printStackTrace();
                    return " vuoto";}
                        }
                );*/
       /* output.toStream().to("Query1",
                Produced.with(Serdes.String(),Serdes.String()));
*/

                /*.toStream().to("Query1");*/

        /*// Write the `KTable<String, Long>` to the output topic.
        wordCounts.toStream().to("streams-wordcount-output",
                Produced.with(Serdes.String(), Serdes.Long()));*/


        // Now that we have finished the definition of the processing topology we can actually run
        // it via `start()`.  The Streams application as a whole can be launched just like any
        // normal Java application that has a `main()` method.
        final KafkaStreams streams = new KafkaStreams(builder.build(), props);

        streams.cleanUp();
        streams.start();

        // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }
}
